const {randomUUID} = require('crypto');
const {Provider1} = require("./mockupApi/provider1/provider1");
const {Provider2} = require("./mockupApi/provider2/provider2");
const input = require('./incomeStorage/home_task_vehicles_input.json');
const {NormalizeData} = require("./helpers/NormalizeData");

const STATUS = {
    INIT:0,
    READY:1,
    PROGRESS:1,
    DONE:1,
    ERROR:4,
    NO_DATA:5,
}

class Processor {
    processes = new Map();
    providers = {
        "provider1": Provider1,
        "provider2": Provider2
    }

    constructor(input) {
        this.initProcess();

        try {
            input.forEach(row => {
                if (this.validateInput(row)) {
                    this.addTask(row);
                }
            })
        } catch (e) {
            console.error(e)
            this.status = STATUS.ERROR;
        }

        if (this.processes.length < 1) {
            this.status = STATUS.NO_DATA;
        }
    }

    async fetchData() {

        let promisesArray = [];
        Array.from(this.processes).forEach(list => {
            let process = JSON.parse(list[1]);

            promisesArray.push(
                this.providers[process.provider_id].getResponse(process.vehicle_id).then(res => {
                    this.set(`${process.provider_id}-${process.vehicle_id}`, {
                        ...process,
                        status:STATUS.READY,
                        response: res
                    })
                })
            )
        });

        await Promise.allSettled(promisesArray);

        // In real life it would be more correct to have a provider based normalizer
        this.report =  new NormalizeData(this.processes).getResponse();

        this.status = STATUS.DONE;
        return this.report;
    }

    initProcess() {
        this.requestId = randomUUID();
        this.status = STATUS.INIT;
    }

    addTask(row) {

        this.set(`${row.provider_id}-${row.vehicle_id}`, {
            ...row,
            status: STATUS.INIT
        })
    }

    get(key) {
        try {
            return JSON.parse(this.processes.get(key));
        } catch (e) {
            return false;
        }
    }

    set(key, value) {
        try {
            return this.processes.set(key, JSON.stringify(value));
        } catch (e) {
            return false;
        }
    }

    validateInput() {
        // todo: add some logic to validate/sanitize input

        return true;
    }

}

(new Processor(input)).fetchData().then(res => {
    console.log('output', res);
})

exports.Processor = Processor;