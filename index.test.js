const {Processor} = require('./index');
const correctResponse = require('./home_task_expected_output.json');
const input = require("./incomeStorage/home_task_vehicles_input.json");

test('Test input process', async () => {
    let currentResponse = await (new Processor(input)).fetchData();
    expect(currentResponse).toMatchObject(correctResponse);
});
