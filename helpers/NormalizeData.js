const KM = {
    mile: {
        multiple: 1.6
    },
    meter: {
        multiple: 0.001
    }
}
const LITER = {
    gallon:{
        multiple: 3.78541178
    },
    liter:{
        multiple: 1
    }
}

class NormalizeData {
    response = [];

    constructor(data) {
        this.data = data;
    }

    getResponse() {

        this.data.forEach(segment => {
            segment = JSON.parse(segment);
            this.response.push({
                vehicle_id: {
                    value: segment.vehicle_id,
                    unit: null
                },
                odometer: this.getOdometer(segment.response.odometer.value,segment.response.odometer.unit),
                fuel_level: this.getFuelLevel(segment.response.fuel_status, segment.response.fuel_capacity),
            })
        })

        return this.response;
    }

    getOdometer(value, unit) {
        try {
            return {
                unit: "kilometer",
                value: +(value * KM[unit].multiple)
            }
        } catch (e) {
            console.error(e)
        }
    }

    getFuelLevel(fuel_status,fuel_capacity){
        let fuelStatus = fuel_status.value * LITER[fuel_status.unit].multiple;
        let fuelCapacity = fuel_capacity.value * LITER[fuel_capacity.unit].multiple;
       try{
           return {
               unit:"percentage",
               value: +(fuelStatus / fuelCapacity) * 100
           }
       }catch (e){
           console.error(e);
           process.exit();
       }
    }
}

exports.NormalizeData = NormalizeData;
//
// [
//     {
//         "vehicle_id": {
//             "value": "vehicle1",
//             "unit": null
//         },
//         "odometer": {
//             "value": 1.6,
//             "unit": "kilometer"
//         },
//         "fuel_level": {
//             "value": 25,
//             "unit": "percentage"
//         }
//     },
//     {
//         "vehicle_id": {
//             "value": "vehicle2",
//             "unit": null
//         },
//         "odometer": {
//             "value": 7.456,
//             "unit": "kilometer"
//         },
//         "fuel_level": {
//             "value": 50,
//             "unit": "percentage"
//         }
//     }
// ]