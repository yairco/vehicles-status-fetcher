const response = require('./home_task_vehicle1_provider1_api_response.json');

class Provider1 {
    static async getResponse(vehicle_id){
        return response;
    }
}

exports.Provider1 = Provider1;