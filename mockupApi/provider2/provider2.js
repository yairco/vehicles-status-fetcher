const response = require('./home_task_vehicle2_provider2_api_response.json');

class Provider2 {
    static async getResponse(vehicle_id){
        return response;
    }
}

exports.Provider2 = Provider2;